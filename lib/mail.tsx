import { StateProvider } from 'apprise-frontend-core/state/provider'
import { PropsWithChildren } from 'react'
import { TopicRegistryContext } from './modules'
import { MailRegistry } from './modules'


export type MailProps = PropsWithChildren


export const initiallRegistry : MailRegistry = {

    modules: []
}

export const Mail = (props: MailProps) => {

    const { children } = props

    return <StateProvider initialState={initiallRegistry} context={TopicRegistryContext}>
        {children}
    </StateProvider>
}