import { useT } from 'apprise-frontend-core/intl/language'
import { useValidation } from 'apprise-ui/field/validation'
import { MailSettings } from './settings'



export const useMailSettingsFields = (settings:MailSettings) => {

    const t = useT()

    const {check,is} = useValidation()

    return {

        quietMode: {

            label: t('mail.quiet_mode_label'),
            msg: t('mail.quiet_mode_msg'),
            help: t('mail.quiet_mode_help')
        }
        ,

        echoMode: {

            label: t('mail.echo_mode_label'),
            msg: t('mail.echo_mode_msg'),
            help: t('mail.echo_mode_help')
        },


        echoAddress: {

            label: t('mail.echo_address_label'),
            msg: t('mail.echo_address_msg'),
            help: t('mail.echo_address_help'),

            ...settings.echoMode ? check(is.empty).and(is.badEmail).on(settings.echoAddress) : {}
        },

        divertMode: {

            label: t('mail.divert_mode_label'),
            msg: t('mail.divert_mode_msg'),
            help: t('mail.divert_mode_help')
        },

        divertAddress: {

            label: t('mail.divert_address_label'),
            msg: t('mail.divert_address_msg'),
            help: t('mail.divert_address_help'),

            ...settings.divertMode ? check(is.empty).and(is.badEmail).on(settings.divertAddress) : {}
        },
    }
}