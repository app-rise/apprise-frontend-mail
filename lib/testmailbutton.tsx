import { useCall } from 'apprise-frontend-core/client/call'
import { useMode } from 'apprise-frontend-core/config/api'
import { useT } from 'apprise-frontend-core/intl/language'
import { classname } from 'apprise-ui/component/model'
import { Tip } from 'apprise-ui/tooltip/tip'
import { DevOnlyIcon } from 'apprise-ui/utils/icons'
import * as React from 'react'
import { AiOutlineMail } from 'react-icons/ai'
import "./testmailbutton.scss"


export const TestMailButton = () => {

    const t = useT()
    const call = useCall()

    const total = React.useRef(-1)
    const [newMessages, newMessagesSet] = React.useState(0)
    const [lastTimeout, lastTimeoutSet] = React.useState<NodeJS.Timeout | undefined>(undefined!)

    const mode = useMode()

    React.useEffect(() => {

        if (lastTimeout || mode.development)
            return

        lastTimeoutSet( setTimeout(async () => {

            const msgs = await call.atPath('/testmail/api/v2/messages').get<{ total: number }>()

            if (total.current===-1 || msgs.total< total.current)
                total.current = msgs.total
            
            newMessagesSet(msgs.total - total.current)
    
            lastTimeoutSet(undefined)

        }, 5000))

        //eslint-disable-next-line
    }, [lastTimeout])

    //console.log({total:total.current,new:newMessages.current})

    const resetNew = () => {
        total.current = -1
        newMessagesSet(0)
    }

    return <div onClick={resetNew}><a href='/testmail' target='_blank' rel='noreferrer' className={classname('testmailbtn', newMessages > 0 && 'btn-newmessages')}>
        <Tip tipPlacement='left' tip={
            <div className='apprise-row'>
                <DevOnlyIcon />
                &nbsp;{t('mail.testmailbtn', { count: newMessages })}
            </div>
        }>

            <AiOutlineMail fill={'bluecadet'} color='white' />
        </Tip>
    </a></div>
}