import { SettingRenderProps, SettingsModule } from 'apprise-frontend-streams/settings/model';
import { Form } from 'apprise-ui/form/form';
import { SwitchBox } from 'apprise-ui/switchbox/switchbox';
import { TextBox } from 'apprise-ui/textbox/textbox';
import { MailIcon, mailType } from './constants';
import { useMailSettingsFields } from './fields';


export type MailSettings = {
    
    quietMode : boolean
    echoMode: boolean
    echoAddress: string | undefined
    divertMode: boolean
    divertAddress: string | undefined
  
}

export const MailSettingsPanel = (props: SettingRenderProps<MailSettings>) => {

    const { onChange, settings } = props

    const fields = useMailSettingsFields(settings)

    return <Form>

        <SwitchBox info={fields.quietMode} onChange={mode => onChange(mode ? {...settings, quietMode: true, echoMode: false, divertMode: false} : {...settings, quietMode: false})} >
            {settings.quietMode}
        </SwitchBox>

        <SwitchBox info={fields.echoMode} onChange={mode => onChange({ ...settings, echoMode: !!mode })} >
            {settings.echoMode}
        </SwitchBox>
        
        <TextBox  info={fields.echoAddress}
            enabled={settings.echoMode} 
            onChange={echoAddress => onChange({ ...settings, echoAddress })}>
            {settings.echoAddress}
        </TextBox>

        <SwitchBox info={fields.divertMode} onChange={mode => onChange({ ...settings, divertMode: !!mode })} >
            {settings.divertMode}
        </SwitchBox>

        <TextBox  info={fields.divertAddress}
            enabled={settings.divertMode} 
            onChange={divertAddress => onChange({ ...settings, divertAddress })}>
            {settings.divertAddress}
        </TextBox>

    </Form>
}

export const mailSettingsModule: SettingsModule<MailSettings> = {

    Icon: MailIcon,
    name: 'mail.singular',
    defaults: {
        quietMode: true,
        echoMode: false,
        divertMode: false,
    },
    type: mailType,
    Render: MailSettingsPanel,
    fields: useMailSettingsFields

}

