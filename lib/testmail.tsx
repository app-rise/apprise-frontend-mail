import { useT } from 'apprise-frontend-core/intl/language'
import { useLogged } from 'apprise-frontend-iam/login/logged'
import { Button } from 'apprise-ui/button/button'
import { useFeedback } from 'apprise-ui/utils/feedback'
import { Fragment } from 'react'
import { useMail } from './api'
import { MailIcon } from './constants'



export const TestMailSection = () => {


    const t = useT()
    const mail = useMail()
    const logged = useLogged()
    const fb = useFeedback()

    const sendMail = async () => {

        mail.sendTo(logged.username)
            .template('personaltest')
            .params({ name: logged.details.firstname ?? logged.username })
            .on({ key: undefined!, value: logged.username, audience: undefined! })
            
        fb.showNotification(t("mail.sent_msg"))
        

    }

    return <Fragment>

        <div className='profile-section-title'>{t('mail.test_title')}</div>
        <div className='profile-section-explainer'>{t('mail.test_explainer')}</div>

        <Button type='primary' icon={<MailIcon />} onClick={sendMail}>
            {t('mail.test_btn')}
        </Button>
    </Fragment>
}